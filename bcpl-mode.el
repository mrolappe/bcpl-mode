;;; bcpl-mode.el --- Major mode for BCPL -*- lexical-binding: t -*-

;; Author: marco rolappe
;; Maintainer: marco rolappe
;; Version: 0.1
;; Homepage: https://codeberg.org/mrolappe/bcpl-mode
;; Keywords: languages


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Major mode for BCPL

;;; Code:

(defconst bcpl-mode/keywords
    (list "ABS" "AND" "BE" "BITSPERBCPLWORD" "BREAK" "BY" "CASE" "DEFAULT" "DO" "ELSE" "ENDCASE" "EQ" "EQV" "EVERY" "EXIT"
          "FALSE" "FINISH" "FIX" "FLOAT" "FOR"
          "GE" "GET" "GLOBAL" "GOTO" "GR" "IF" "INTO" "LE" "LET" "LOGAND" "LOGOR" "LOOP" "LS" "LSHIFT" "LV"
          "MANIFEST" "MATCH" "MOD" "NE" "NEEDS" "NEQV" "NEXT" "NOT" "OF" "OR"
          "REPEAT" "REPEATWHILE" "REPEATUNTIL" "RESULTIS" "REM" "RETURN" "RSHIFT" "RV"
          "SECTION" "SKIP" "SLCT" "STATIC" "SWITCHON"
          "TABLE" "TEST" "THEN" "TO" "TRUE" "UNLESS" "UNTIL" "VALOF" "VEC" "WHILE" "XOR"))

(defconst bcpl-mode/keywords-regexp
    (list (regexp-opt bcpl-mode/keywords 'symbols)))

(defconst bcpl-mode/operators
    (list "<<" ">>"     ;; left shift, right shift
          "~", "&", "|" ;; not, and, or
          "NEQV", "EQV" ;; not-equivalence (xor), equivalence
          ))

(defun bcpl-mode/tree-sitter-setup ()
    (setq tree-sitter-default-patterns
          [ (vconcat bcpl-mode/keywords) @keyword
            ]))

(defun bcpl-mode/additional-setup ()
    (set (make-local-variable 'imenu-generic-expression) '((nil "\\(let\\|and\\)\s+\\(.+\\)(.*)\s+\\(=\\|be\\)" 2)))

    ;; see modify-syntax-entry documentation for meaning of the flags
    (modify-syntax-entry ?/ ". 124b")
    (modify-syntax-entry ?* ". 23")
    (modify-syntax-entry ?\n "> b"))

;;;###autoload
(define-derived-mode bcpl-mode prog-mode "BCPL"
    "Major mode for BCPL"
    :group 'bcpl-mode

    (let ((keywords-only nil)
          (case-fold t))

        (setq font-lock-defaults `(bcpl-mode/keywords-regexp ,keywords-only ,case-fold)))

    (bcpl-mode/additional-setup))

(provide 'bcpl-mode)

;;; bcpl-mode.el ends here
